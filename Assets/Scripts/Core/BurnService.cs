﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class BurnService : MonoBehaviour {
    //list of ground components
    public CtrGround[] grounds;
    //interval in which another ground will be set on fire
    public int INTERVAL = 180;

    private int tics;
    private bool isAllBurned = false;

    /**
     * {@inheritdoc}
     */
    private void Start()
    {
        this.tics = this.INTERVAL;
    }

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        if (!this.isAllBurned)
        {
            this.tics--;
            if (this.tics <= 0)
            {
                this.burnGround();
            }
        }
    }

    /**
     * Burns all grounds until none remains 
     */
    private void burnGround()
    {
        int chosenGround = Random.Range(0, this.grounds.Length);
        if (!this.grounds[chosenGround].isOnFire)
        {
            this.grounds[chosenGround].setOnFire();
            this.tics = this.INTERVAL;
        }
        else
        {
            //check if all grounds are on fire
            foreach (CtrGround ground in this.grounds)
            {
                //if at least one is not fire return
                if (!ground.isOnFire)
                {
                    return;
                }
            }
            //if all on fire, stop burning others
            this.isAllBurned = true;
        }
    }
}
