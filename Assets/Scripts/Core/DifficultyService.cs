﻿using UnityEngine;
using System.Collections.Generic;

/**
 * @author Luboš Maťha
 */
public static class DifficultyService {
    public const string HP = "HP";
    public const string MAX_HP = "MAX_HP";
    public const string BURN_INTERVAL = "BURN_INTERVAL";
    public const string JUMP_FORCE = "JUMP_FORCE";
    public const string PLAYER_DMG = "PLAYER_DMG";

    private static int difficulty = 2;

    /**
     * initializes the difficulty value with default 2
     */
    public static void init()
    {
        DifficultyService.setDifficulty(PlayerPrefs.GetInt("difficulty", 2));
    }

    public static Dictionary<string, int> getDifficultySettings()
    {
    
        Dictionary<string, int> settings = new Dictionary<string, int>();
        switch (DifficultyService.difficulty)
        {
                //easy
            case 1:
                settings.Add(DifficultyService.HP, 3);
                settings.Add(DifficultyService.MAX_HP, 3);
                settings.Add(DifficultyService.BURN_INTERVAL, 240);
                settings.Add(DifficultyService.JUMP_FORCE, 450);
                settings.Add(DifficultyService.PLAYER_DMG, 2);
                return settings;
                //medium
            case 2:
                settings.Add(DifficultyService.HP, 2);
                settings.Add(DifficultyService.MAX_HP, 2);
                settings.Add(DifficultyService.BURN_INTERVAL, 180);
                settings.Add(DifficultyService.JUMP_FORCE, 350);
                settings.Add(DifficultyService.PLAYER_DMG, 1);
                return settings;
                //hard
            case 3:
                settings.Add(DifficultyService.HP, 1);
                settings.Add(DifficultyService.MAX_HP, 1);
                settings.Add(DifficultyService.BURN_INTERVAL, 60);
                settings.Add(DifficultyService.JUMP_FORCE, 300);
                settings.Add(DifficultyService.PLAYER_DMG, 0);
                return settings;
            default:
                return settings;
        }
    }

    public static void setDifficulty(int difficulty)
    {
        PlayerPrefs.SetInt("difficulty", difficulty);
        DifficultyService.difficulty = difficulty;
    }

    public static int getDifficulty()
    {
        return DifficultyService.difficulty;
    }
}
