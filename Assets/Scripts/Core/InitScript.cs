﻿using UnityEngine;
using System.Collections;

/**
 * used for inicialization at the beggining of the scene
 * 
 * @author Luboš Maťha
 */
public class InitScript : MonoBehaviour {

    public CtrKoblizek koblizek;

    public BurnService burnService;

    /**
     * {@inheritdoc}
     */
	private void Awake () {
        StaticLoader.init();
        DifficultyService.init();
        if (StaticLoader.isNewGame() && Application.loadedLevel > 0) 
        {
            this.koblizek.HP = StaticLoader.getGameSettings()[DifficultyService.HP];
            this.koblizek.MAX_HP = StaticLoader.getGameSettings()[DifficultyService.MAX_HP];
            this.koblizek.JUMP_FORCE = StaticLoader.getGameSettings()[DifficultyService.JUMP_FORCE];
            this.koblizek.DMG = StaticLoader.getGameSettings()[DifficultyService.PLAYER_DMG];
            if (this.burnService)
            {
                this.burnService.INTERVAL = StaticLoader.getGameSettings()[DifficultyService.BURN_INTERVAL];
            }
        }
	}
}
