﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public abstract class LocalizationComponent : MonoBehaviour {
    public int langId;

    abstract public void setMenuTitle(int difficulty);
}
