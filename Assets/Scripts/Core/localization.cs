﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

/**
 * used for localizating of all objects stored in texts and localizationComponents
 * 
 * @author Luboš Maťha
 */
public class localization : MonoBehaviour {
    public TextComponent[] texts;

    public LocalizationComponent[] localizationComponents;

    /**
     * {@inheritdoc}
     */
	private void Awake ()
    {
        this.changeTexts();
	}

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        if (StaticLoader.getLanguageChanged())
        {
            this.changeTexts();
            StaticLoader.setLanguageChanged(false);
        }
    }

    private void changeTexts()
    {
        foreach (TextComponent textComponent in texts)
        {
            string query =
                "SELECT value FROM Translation WHERE lang_id = " +
                "(SELECT lang_id FROM Player WHERE active = 1) AND " +
                "loc_key_id = (SELECT id FROM LocalizationKey WHERE value = '{0}');";
            //tries to translate text
            if (textComponent.text != null)
            {
                DataTable table = StaticLoader.db.ExecuteQuery(String.Format(query, textComponent.key));
                foreach (DataRow row in table.Rows)
                {
                    textComponent.text.text = (string)row["value"];
                    break;
                }
            }
            //tries to translate image
            if (textComponent.image != null)
            {
                DataTable table = StaticLoader.db.ExecuteQuery(String.Format(query, textComponent.key));
                foreach (DataRow row in table.Rows)
                {
                    textComponent.image.sprite = Resources.Load<Sprite>((string)row["value"]);
                    break;
                }
            }
        }

        foreach (LocalizationComponent component in this.localizationComponents)
        {
            string langIdQuery = "SELECT lang_id FROM Player WHERE active = 1";
            DataTable table = StaticLoader.db.ExecuteQuery(langIdQuery);
            foreach (DataRow row in table.Rows)
            {
                component.langId = (int)row["lang_id"];
                break;
            }
        }
        
    }
}

[System.Serializable]
public class TextComponent : System.Object {
	public string key;
	public Text text;
    public Image image;
}
