﻿using UnityEngine;
using System.Collections.Generic;
using System;

/**
 * @author Luboš Maťha
 */
public static class StaticLoader {
    //connection to sqlite database
    public static SqliteDatabase db = new SqliteDatabase("data.sqlite");
    //info about change of language
    private static bool languageChanged = false;
    //info about change of saved games
    private static bool savedGamesChanged = false;
    //temporary saved data for game
    private static string gameToLoad;
    //stores info about if this game was loaded or started as new
    private static bool newGame = true;
    //difficulty settings for loaded level
    private static Dictionary<string, int> gameSettings;

    /**
     * initializes and loades the basic values of StaticLoader
     */
    public static void init()
    {
        StaticLoader.newGame = true;
        if (StaticLoader.gameToLoad != null)
        {
            LevelSerializer.LoadNow(StaticLoader.gameToLoad);
            StaticLoader.newGame = false;
            StaticLoader.gameToLoad = null;
        }
        StaticLoader.setSoundVolume(PlayerPrefs.GetFloat("sound_volume", 0.5f));
    }

    public static bool isNewGame()
    {
        return StaticLoader.newGame;
    }

    public static void loadGame(string level, string data)
    {
        Application.LoadLevel(level);
        StaticLoader.gameToLoad = data;
    }

    public static void loadGame(int level, string data)
    {
        Application.LoadLevel(level);
        StaticLoader.gameToLoad = data;
    }

    /**
     * sets the volume of all audio sources in game [0;1]
     */
    public static void setSoundVolume(float soundVolume)
    {
        PlayerPrefs.SetFloat("sound_volume", soundVolume);
        AudioListener.volume = soundVolume;
    }

    public static float getSoundVolume()
    {
        return PlayerPrefs.GetFloat("sound_volume", 0.5f);
    }

    public static void setLanguage(int langId)
    {
        string query = "UPDATE Player SET lang_id={0}";
        StaticLoader.db.ExecuteNonQuery(String.Format(query, langId));
        StaticLoader.languageChanged = true;
    }

    public static bool getLanguageChanged()
    {
        return StaticLoader.languageChanged;
    }

    public static void setLanguageChanged(bool changed)
    {
        StaticLoader.languageChanged = changed;
    }

    public static bool getSavedGamesChanged()
    {
        return StaticLoader.savedGamesChanged;
    }

    public static void setSavedGamesChanged(bool changed)
    {
        StaticLoader.savedGamesChanged = changed;
    }

    public static void unlockLevel(int levelId)
    {
        string query = "UPDATE PlayerLevel SET active=1 WHERE level_id = {0}";
        StaticLoader.db.ExecuteNonQuery(String.Format(query, levelId));
    }

    public static void setGameSettings(Dictionary<string, int> settings)
    {   
        StaticLoader.gameSettings = settings;
    }

    public static Dictionary<string, int> getGameSettings()
    {
        return StaticLoader.gameSettings;
    }
}

