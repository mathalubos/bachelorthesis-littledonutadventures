﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/**
 * @author Luboš Maťha
 */
public class CtrVolumeSlider : MonoBehaviour {

    public Slider volumeSlider;

    /**
     * {@inheritdoc}
     */
    void Start()
    {
        this.volumeSlider.value = StaticLoader.getSoundVolume();
    }

    public void onVolumeChange(float volume)
    {
        StaticLoader.setSoundVolume(volume);
    }
}
