﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrFoxCage : MonoBehaviour {
    /**
     * reacts on collision with fox
     */
    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the fox enters the trigger zone...
        if (other.tag == "fox")
        {
            //flip it
            other.GetComponent<CtrFox>().flip();
        }
    }
}
