﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

/**
 * Class used to controll the hero
 * 
 * @author Luboš Maťha
 */
public class CtrKoblizek : MonoBehaviour {
    public float maxSpeed = 10f;    
    public bool facingRight = true;
    public bool grounded = false;
    public bool doubleJump = false;    
    public float groundRadius = 0.2f;    
    public int BerryCount = 0;
    public AudioClip[] dmgClips;
    public AudioClip gameOverClip;
    public AudioClip vicotryClip;
    public bool playable = true;
    public Text berryCountText;
    public Transform groundCheck;
    public LayerMask ground; 
    public GameObject gameOverText;
    public Camera followingCamera;
    public Image[] healtImages;
    public int HP = 3;
    public int MAX_HP = 3;
    public int JUMP_FORCE = 350;
    public int DMG = 1;
    public bool endOfGame = false;
    public CtrLevelMenuButton menu;
    public Rigidbody2D fireBall;
    
    private Vector3 respawnPosition;    
    private int disableFor = 0;
    private Animator anim;
    private bool updateHP = true;

    /**
     *  adds one berry to player
     */
    public void addBerry()
    {
        this.BerryCount++;
        this.berryCountText.text = this.BerryCount.ToString();
        if (this.BerryCount == 6)
        {
            this.endOfGame = true;
            AudioSource.PlayClipAtPoint(this.vicotryClip, this.followingCamera.transform.position);
            StaticLoader.unlockLevel(Application.loadedLevel + 1);
        }
    }

    /**
     * disables/enables controlling of hero; can enable only if !this.endOfGame
     */
    public void setPlayable(bool playable)
    {
        if (playable && this.endOfGame)
        {
            this.playable = false;
            return;
        }
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        this.anim.SetFloat("move", 0);
        this.anim.SetFloat("vSpeed", 0);
        this.playable = playable;
    }

    /**
     * returns if player is controllable
     */
    public bool isPlayable()
    {
        return this.playable;
    }

    /**
     * returns true if player took dmg and still lives otherwise rerurns false
     **/
    public bool takeDmg(int dmg, bool recoil)
    {
        if (this.endOfGame)
        {
            return false;
        }
        //if hero takes dmg, player cannot controll him for 30 frames
        this.disableFor = 30;
        this.playDmgClip();
        this.HP -= dmg;
        this.repaintHealth();
        if (this.HP <= 0)
        {
            if (this.followingCamera)
            {
                AudioSource.PlayClipAtPoint(this.gameOverClip, this.followingCamera.transform.position);
            }
            else
            {
                AudioSource.PlayClipAtPoint(this.gameOverClip, transform.position);
            }
            
            this.anim.SetTrigger("dead");
            if (this.gameOverText)
            {
                this.gameOverText.SetActive(true);
            }            
            this.setPlayable(false);
            this.endOfGame = true;
            return false;
        }
        if (recoil)
        {
            float sideForce = 600f;
            //choose the side player should be recoiled
            if (this.facingRight)
            {
                sideForce *= -1f;
            }
            GetComponent<Rigidbody2D>().AddForce(new Vector2(sideForce, this.JUMP_FORCE));
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    private void Awake()
    {
        this.anim = GetComponent<Animator>();        
    }

    /**
     * {@inheritdoc}
     */
	private void Start () 
    {        
        this.respawnPosition = transform.localPosition;
        this.anim.SetBool("grounded", false);
        this.updateHP = true;
	}

    /**
     * {@inheritdoc}
     */
    private void FixedUpdate()
    {
        //check if on the ground
        this.grounded = Physics2D.OverlapCircle(this.groundCheck.position, this.groundRadius, this.ground);
        this.anim.SetBool("grounded", this.grounded);
        //if disabled do not set any input
        if (!this.playable || this.disableFor > 0)
        {
            this.disableFor--;
            return;
        }
        
        //set vertical speed for jump animation
        this.anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        //show victory text and disable player
        if (this.BerryCount == 6 || this.endOfGame)
        {
            this.endOfGame = true;
            this.menu.stopGame();
            AudioSource.PlayClipAtPoint(this.vicotryClip, this.followingCamera.transform.position);
            this.menu.changeMenu("next_level");
            StaticLoader.unlockLevel(Application.loadedLevel+1);
            return;
        }

        //restart double jump option
        if (this.doubleJump && this.grounded)
        {
            this.doubleJump = false;
        }
        //get which way hero should go
        float speed = Input.GetAxis("Horizontal");
        //and set it to rigidbody
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed * this.maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        //we dont want the walk animation while jumping
        if (!this.grounded)
        {
            return;
        }
        this.anim.SetFloat("move", Mathf.Abs(speed));

        //flip hero in the direction of walk
        if (speed > 0 && !facingRight) 
        {
            this.flip();
        } else if (speed < 0 && facingRight) 
        {
            this.flip();
        }
    }

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        if (this.updateHP)
        {
            if (this.repaintHealth())
            {
                this.updateHP = false;
            }            
        }
        if (!this.playable || this.disableFor > 0)
        {
            return;
        }

        // If the fire button is pressed...
        if (Input.GetButtonDown("Fire1") && this.DMG > 0 && Application.loadedLevel >= 3)
        {
            Vector3 spawnPosition = transform.position;
            this.fireBall.GetComponent<CtrShot>().dmg = this.DMG;
            // If the player is facing right...
            if (this.facingRight)
            {
                spawnPosition.x += 1;
                // ... instantiate the rocket facing right and set it's velocity to the right. 
                Rigidbody2D bulletInstance = Instantiate(this.fireBall, spawnPosition, Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
                bulletInstance.velocity = new Vector2(10, 0);
            }
            else
            {
                spawnPosition.x -= 1;
                // Otherwise instantiate the rocket facing left and set it's velocity to the left.
                Rigidbody2D bulletInstance = Instantiate(this.fireBall, spawnPosition, Quaternion.Euler(new Vector3(0, 0, 180f))) as Rigidbody2D;
                bulletInstance.velocity = new Vector2(-10, 0);
            }
        }

        if ((this.grounded || !this.doubleJump) && Input.GetKeyDown(KeyCode.Space) && this.playable)
        {
            // Add a vertical force to the player.
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, this.JUMP_FORCE));
            if (!this.doubleJump && !this.grounded)
            {
                this.doubleJump = true;
            }
        }

        if (transform.localPosition.y < -100)
        {
            if (this.takeDmg(1, false))
            {
                //stop the hero before respawning
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                this.anim.SetFloat("move", 0);
                this.anim.SetFloat("vSpeed", 0);
                this.respawn();
            }
            else
            {
                GameObject.Destroy(gameObject);
            }            
        }
    }

    /**
     * turns around the player 
     */
    private void flip()
    {
        // Switch the way the player is labelled as facing.
        this.facingRight = !this.facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    /**
     * plays random dmg clip, ouch
     **/
    private void playDmgClip()
    {
        var i = Random.Range(0, this.dmgClips.Length);
        AudioSource.PlayClipAtPoint(this.dmgClips[i], transform.position);
    }

    /**
     * respawns hero to the beggining of game
     **/
    private void respawn()
    {
        transform.localPosition = this.respawnPosition;
        if (this.followingCamera)
        {
            this.followingCamera.GetComponent<CameraFollow>().enabled = true;
        }        
    }

    /**
     * repaints the hears by the amount of HP
     **/
    private bool repaintHealth()
    {
        if (this.healtImages.GetValue(0) == null)
        {
            return false;
        }
        for (int i = 0; i < this.healtImages.Length; i++)
        {            
            if (i < this.MAX_HP && i < this.HP)
            {
                this.healtImages[i].color = Color.red;
            }
            else if (i < this.MAX_HP)
            {
                this.healtImages[i].color = Color.white;
            }
            else
            {
                this.healtImages[i].color = new Color(0, 0, 0, 0);
            }
        }
        return true;
    }
}
