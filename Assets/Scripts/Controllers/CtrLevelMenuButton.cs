﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrLevelMenuButton : MonoBehaviour {
    public GameObject ui;
    public GameObject mainMenu;
    public GameObject optionMenu;
    public GameObject saveLoadMenu;
    public GameObject storyMenu;
    public GameObject nextLevelMenu;
    public CtrKoblizek player;

    public void stopGame()
    {
        this.ui.SetActive(true);
        if (this.player)
        {
            this.player.setPlayable(false);
        }
    }

    public void continueGame()
    {
        this.mainMenu.SetActive(true);
        this.optionMenu.SetActive(false);
        this.saveLoadMenu.SetActive(false);
        this.storyMenu.SetActive(false);
        this.nextLevelMenu.SetActive(false);
        this.ui.SetActive(false);
        if (this.player)
        {
            this.player.setPlayable(true);
        }        
    }

    public void endGame()
    {
        Application.LoadLevel(0);
    }

    public void changeMenu(string menu)
    {
        switch (menu)
        {
            case "options":
                this.optionMenu.SetActive(true);
                this.mainMenu.SetActive(false);
                this.saveLoadMenu.SetActive(false);
                this.storyMenu.SetActive(false);
                this.nextLevelMenu.SetActive(false);
                break;
            case "main":
                this.optionMenu.SetActive(false);
                this.mainMenu.SetActive(true);
                this.saveLoadMenu.SetActive(false);
                this.storyMenu.SetActive(false);
                this.nextLevelMenu.SetActive(false);
                break;
            case "save_load":
                this.optionMenu.SetActive(false);
                this.mainMenu.SetActive(false);
                this.saveLoadMenu.SetActive(true);
                this.storyMenu.SetActive(false);
                this.nextLevelMenu.SetActive(false);
                break;
            case "story":
                this.optionMenu.SetActive(false);
                this.mainMenu.SetActive(false);
                this.saveLoadMenu.SetActive(false);
                this.storyMenu.SetActive(true);
                this.nextLevelMenu.SetActive(false);
                break;
            case "next_level":
                this.optionMenu.SetActive(false);
                this.mainMenu.SetActive(false);
                this.saveLoadMenu.SetActive(false);
                this.storyMenu.SetActive(false);
                this.nextLevelMenu.SetActive(true);
                break;
            case "leave":
                this.continueGame();
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    private void Start()
    {
        if (StaticLoader.isNewGame())
        {
            this.stopGame();
            this.changeMenu("story");
        }
    }

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (this.ui.activeSelf)
            {
                this.continueGame();
            }
            else
            {
                this.stopGame();
            }
        }
    }
}
