﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/**
 * This class is used for dynamic loading of load level buttons and new game buttons
 * 
 * @author Luboš Maťha
 */
public class ButtonLoader : LocalizationComponent {
    /**
     * The type of the button loader [load, levels]
     */
    public string type;

    /**
     * inherited method used for localization
     */
    public override void setMenuTitle(int difficulty)
    {
        this.repaintButtons();
    }
    /**
     * used for button inicialization
     */
	private void Start ()
    {
        this.repaintButtons();
	}
	
    /**
     * repaints buttons if some new saved games were added
     */
	private void Update ()
    {
        if (StaticLoader.getSavedGamesChanged())
        {
            this.repaintButtons();
            StaticLoader.setSavedGamesChanged(false);
        }        
	}

    /**
     * destroys old and builds new level buttons
     */
    private void repaintButtons()
    {
        this.clearButtons();
        switch (this.type) {
            case "load":
                for (int i = 0; i < LevelSerializer.SavedGames[LevelSerializer.PlayerName].Count; i++)
                {
                    LevelSerializer.SaveEntry sg = LevelSerializer.SavedGames[LevelSerializer.PlayerName][i];
                    GameObject dynamicButton;
                    dynamicButton = Instantiate(Resources.Load<GameObject>("DynamicLoadButton")) as GameObject;
                    dynamicButton.GetComponentInChildren<Text>().text = sg.Caption;
                    dynamicButton.GetComponent<CtrLoadSaveGame>().setLevel(sg.Level);
                    dynamicButton.GetComponent<CtrLoadSaveGame>().setData(sg.Data);
                    dynamicButton.GetComponent<CtrLoadSaveGame>().setGameId(i);
                    dynamicButton.transform.SetParent(this.transform);
                    dynamicButton.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                break;
            case "levels":
                string query = 
                    "SELECT Level.display_name as display_name, Level.name as name FROM"+
                    " PlayerLevel JOIN Level ON PlayerLevel.level_id=Level.id WHERE"+
                    " PlayerLevel.active = 1";
                DataTable table = StaticLoader.db.ExecuteQuery(query);
                foreach (DataRow row in table.Rows)
                {
                    GameObject dynamicButton;
                    dynamicButton = Instantiate(Resources.Load<GameObject>("DynamicLevelButton")) as GameObject;
                    dynamicButton.GetComponentInChildren<Text>().text = this.getLevelName((string)row["name"]);
                    dynamicButton.GetComponent<CtrLoadSaveGame>().setLevel((string)row["name"]);
                    dynamicButton.GetComponent<CtrLoadSaveGame>().setData(null);
                    dynamicButton.transform.SetParent(this.transform);
                    dynamicButton.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                break;
        }      

    }

    /**
     * returns the level name in correct language
     */
    private string getLevelName(string name)
    {
        switch (name)
        {
            case "level1":
                if (this.langId == 1)
                {
                    return "Temný les";
                }
                else
                {
                    return "Dark forest";
                }
            case "level2":
                if (this.langId == 1)
                {
                    return "Hořící les";
                }
                else
                {
                    return "Burning forest";
                }
            case "level3":
                if (this.langId == 1)
                {
                    return "Jeskyně";
                }
                else
                {
                    return "Caves";
                }
            default:
                return "level";
        }
    }

    /**
     * removes old buttons from container
     */
    private void clearButtons()
    {
        foreach (Transform child in this.transform)
        {
            child.gameObject.SetActive(false);
            GameObject.Destroy(child.gameObject);
        }
    }
}
