﻿using UnityEngine;
using System.Collections;
/**
 * handles if player falls over
 * 
 * @author Luboš Maťha
 */
public class CtrEmptyFall : MonoBehaviour {
    public AudioClip fallClip;
    public Camera followingCamera;

    /**
     * if player falls over fall clip is played and camera is detached
     */
    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the player enters the trigger zone...
        if (other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(fallClip, transform.position);
            this.followingCamera.GetComponent<CameraFollow>().enabled = false;
        }
    }
}
