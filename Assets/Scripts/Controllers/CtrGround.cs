﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrGround : MonoBehaviour {
    public GameObject fire;
    public AudioClip fireClip;
    public Material burnedWoodMaterial;
    public bool isOnFire = false;

    /**
     * sets the ground on fire
     */
    public void setOnFire()
    {
        this.isOnFire = true;
        AudioSource.PlayClipAtPoint(this.fireClip, transform.position);
        this.fire.SetActive(true);
        gameObject.GetComponent<MeshRenderer>().material = this.burnedWoodMaterial;
    }

    private void Start()
    {
        if (this.isOnFire)
        {
            this.setOnFire();
        }
    }
}
