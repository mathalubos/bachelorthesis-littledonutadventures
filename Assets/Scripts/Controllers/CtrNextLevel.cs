﻿using UnityEngine;
using System.Collections.Generic;

/**
 * @author Luboš Maťha
 */
public class CtrNextLevel : MonoBehaviour {

    public CtrKoblizek koblizek;

    public void loadNextLevel()
    {
        Dictionary<string, int> settings = DifficultyService.getDifficultySettings();
        settings[DifficultyService.HP] = koblizek.HP;
        settings[DifficultyService.MAX_HP] = koblizek.MAX_HP;
        settings[DifficultyService.JUMP_FORCE] = koblizek.JUMP_FORCE;
        settings[DifficultyService.PLAYER_DMG] = koblizek.DMG;
        StaticLoader.setGameSettings(settings);
        if (Application.loadedLevel < 3)
        {
            StaticLoader.loadGame(Application.loadedLevel + 1, null);
        } else {
            StaticLoader.loadGame(0, null);
        }
    }
}
