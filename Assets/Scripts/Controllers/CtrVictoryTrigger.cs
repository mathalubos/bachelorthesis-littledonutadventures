﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrVictoryTrigger : MonoBehaviour {
    /**
     * reacts on collision with player
     * 
     * sets victory
     */
    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the player enters the trigger zone...
        if (other.tag == "Player")
        {
            // set victory
            other.GetComponent<CtrKoblizek>().endOfGame = true;
        }
    }
}
