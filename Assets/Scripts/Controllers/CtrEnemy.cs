﻿using UnityEngine;
using System.Collections;
/**
 * Deals dmg to player on contact
 */
public class CtrEnemy : MonoBehaviour {
    /**
     * reacts on collision with player
     */
    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the player enters the trigger zone...
        if (other.tag == "Player")
        {
            //deal 1 dmg to him
            other.GetComponent<CtrKoblizek>().takeDmg(1, true);
        }
    }
}
