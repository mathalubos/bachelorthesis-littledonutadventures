﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/**
 * used for controlling difficulty dropdown, is localized
 * 
 * @author Luboš Maťha
 */
public class CtrDifficultyDropdown : LocalizationComponent, IPointerUpHandler, IPointerExitHandler {

    public RectTransform container;
    public Text selectedDifficulty;
    private bool isOpen;

    /**
     * {@inheritdoc}
     */
    public void OnPointerUp(PointerEventData eventData)
    {
        this.isOpen = true;
    }

    /**
     * {@inheritdoc}
     */
    public void OnPointerExit(PointerEventData eventData)
    {
        this.isOpen = false;
    }

    public void OnClick(int difficulty)
    {
        this.isOpen = false;
        this.setMenuTitle(difficulty);
        DifficultyService.setDifficulty(difficulty);
    }

    public override void setMenuTitle(int difficulty)
    {
        switch (difficulty)
        {
            case 1:
                if (this.langId == 1)
                {
                   this.selectedDifficulty.text = "Lehké";
                        break;
                }
                else
                {
                    this.selectedDifficulty.text = "Easy";
                    break;
                }
            case 2:
                if (this.langId == 1)
                {
                    this.selectedDifficulty.text = "Střední";
                    break;
                }
                else
                {
                    this.selectedDifficulty.text = "Medium";
                    break;
                }
            case 3:
                if (this.langId == 1)
                {
                    this.selectedDifficulty.text = "Těžké";
                    break;
                }
                else
                {
                    this.selectedDifficulty.text = "Hard";
                    break;
                }
            default:
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    private void Start()
    {
        this.isOpen = false;
        this.setMenuTitle(DifficultyService.getDifficulty());
    }

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        Vector3 scale = this.container.localScale;
        scale.y = Mathf.Lerp(scale.y, this.isOpen ? 1 : 0, Time.deltaTime * 12);
        this.container.localScale = scale;
    }
}
