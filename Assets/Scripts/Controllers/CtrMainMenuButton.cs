﻿using UnityEngine;
using System;
using UnityEngine.UI;

/**
 * @author Luboš Maťha
 */
public class CtrMainMenuButton : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionMenu;
    public GameObject levelMenu;
    public GameObject loadMenu;
    public GameObject creditsMenu;

    public InputField playerName;

    public void endGame()
    {
        Application.Quit();
    }

    public void changePlayerName(string name)
    {
        name = WWW.EscapeURL(name);
        string query = "UPDATE Player SET name='{0}';";

        StaticLoader.db.ExecuteNonQuery(String.Format(query, name));
    }

    public void changeMenu(string menu)
    {
        switch (menu)
        {
            case "options":
                this.optionMenu.SetActive(true);
                this.levelMenu.SetActive(false);
                this.mainMenu.SetActive(false);
                this.loadMenu.SetActive(false);
                DataTable table = StaticLoader.db.ExecuteQuery("Select name From Player");
                foreach (DataRow row in table.Rows)
                {
                    this.playerName.text = WWW.UnEscapeURL(row["name"].ToString());
                    break;
                }
                break;
            case "levels":
                this.optionMenu.SetActive(false);
                this.levelMenu.SetActive(true);
                this.mainMenu.SetActive(false);
                this.loadMenu.SetActive(false);
                this.creditsMenu.SetActive(false);
                break;
            case "main":
                this.optionMenu.SetActive(false);
                this.levelMenu.SetActive(false);
                this.mainMenu.SetActive(true);
                this.loadMenu.SetActive(false);
                this.creditsMenu.SetActive(false);
                break;
            case "load":
                this.optionMenu.SetActive(false);
                this.levelMenu.SetActive(false);
                this.mainMenu.SetActive(false);
                this.loadMenu.SetActive(true);
                this.creditsMenu.SetActive(false);
                break;
            case "credits":
                this.optionMenu.SetActive(false);
                this.levelMenu.SetActive(false);
                this.mainMenu.SetActive(false);
                this.loadMenu.SetActive(false);
                this.creditsMenu.SetActive(true);
                break;
        }
    }
}
