﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrFox : MonoBehaviour {
    private bool facingLeft = true;
    private int speed = 5;
    private int hp = 2;

    public void takeDmg(int dmg)
    {
        this.hp -= dmg;
        if (this.hp <= 0)
        {
            GameObject.Destroy(gameObject);
        }
    }

    /**
     * turns around the player 
     */
    public void flip()
    {
        // Switch the way the player is labelled as facing.
        this.facingLeft = !this.facingLeft;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(this.getSpeed(), GetComponent<Rigidbody2D>().velocity.y);
    }

    /**
     * returns the speed value according to direction facing
     */
    private int getSpeed()
    {
        if (this.facingLeft)
        {
            return -this.speed;
        }
        else
        {
            return this.speed;
        }
    }

    /**
     * reacts on collision with shot
     */
    private void OnTriggerEnter2D(Collider2D other)
    {        
        // If the fox enters the trigger zone...
        if (other.tag == "shot")
        {
            //deal dmg to it
            this.takeDmg(other.GetComponent<CtrShot>().dmg);
            GameObject.Destroy(other.gameObject);
        }
    }
}
