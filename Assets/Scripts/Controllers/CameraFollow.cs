﻿using UnityEngine;
using System.Collections;

/**
 * This script makes camera on which it is atached follow the player
 * 
 * @author Luboš Maťha
 */
public class CameraFollow : MonoBehaviour 
{
    public float xMargin = 1f;		// Distance in the x axis the player can move before the camera follows.
    public float yMargin = 1f;		// Distance in the y axis the player can move before the camera follows.
    public Transform target;
    public Camera followingCamera;

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        this.followingCamera.orthographicSize = (Screen.height / 100f);
        if (this.target)
        {
            if (this.CheckXMargin() || this.CheckYMargin())
            {
                transform.position = Vector3.Lerp(transform.position, this.target.position, 0.1f) + new Vector3(0, 0, -1);
            }            
        }
    }

    private bool CheckXMargin()
    {
        // Returns true if the distance between the camera and the player in the x axis is greater than the x margin.
        return Mathf.Abs(transform.position.x - target.position.x) > xMargin;
    }


    private bool CheckYMargin()
    {
        // Returns true if the distance between the camera and the player in the y axis is greater than the y margin.
        return Mathf.Abs(transform.position.y - target.position.y) > yMargin;
    }
}
