﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrBlueberry : MonoBehaviour {
    public AudioClip pickupClip;
    public Sprite berrySprite;

    /**
     * used for init of berry sprite because of game loader problems
     */
    private void Start()
    {
        if (GetComponent<SpriteRenderer>().sprite == null)
        {
            GetComponent<SpriteRenderer>().sprite = this.berrySprite;
        }
    }

    /**
     * reacts on collision with player
     * adds berry and destroys itself
     */
    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the player enters the trigger zone...
        if (other.tag == "Player")
        {
            // ... play the pickup sound effect.
            AudioSource.PlayClipAtPoint(pickupClip, transform.position);

            // Increase the number of bombs the player has.
            other.GetComponent<CtrKoblizek>().addBerry();

            // Destroy the berry.
            Destroy(transform.root.gameObject);
        }
    }
}
