﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrShot : MonoBehaviour {
    public int dmg = 1;
    private int durance = 60;

    /**
     * destroy shot after 100 frames
     */
    private void Update()
    {
        this.durance--;
        if (this.durance <= 0)
        {
            GameObject.Destroy(gameObject);
        }
    }
}
