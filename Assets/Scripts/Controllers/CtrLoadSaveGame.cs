﻿using UnityEngine;
using System.Collections;

/**
 * @author Luboš Maťha
 */
public class CtrLoadSaveGame : MonoBehaviour {
    [SerializeField]
    private string level;
    [SerializeField]
    private string data;
    [SerializeField]
    private int gameId;

    /**
     * Loads required level. With saved data or without and with difficulty settings.
     * If with data, difficulty settings are further ignored
     **/
    public void load()
    {
        StaticLoader.setGameSettings(DifficultyService.getDifficultySettings());
        StaticLoader.loadGame(
            this.level,
            this.data
            );
    }

    /**
     *  Saves current scene with information about the scene and player
     *  to LevelSerializer and FilePrefs 
     **/
    public void save()
    {
        GameObject player = GameObject.Find("koblizek");
        /** enable Player during game saving */
        if (player)
        {
            player.GetComponent<CtrKoblizek>().setPlayable(true);
        }
        /** only 4 games can be saved */
        if (LevelSerializer.SavedGames[LevelSerializer.PlayerName].Count == 4)
        {
            return;
        }
        /** get the player name */
        string playerName = "";
        DataTable table = StaticLoader.db.ExecuteQuery("Select name From Player");
        foreach (DataRow row in table.Rows)
        {
            playerName = WWW.UnEscapeURL(row["name"].ToString());
            break;
        }
        /** save current scene */
        LevelSerializer.SaveGame(playerName);
        /** inform listeners that saved games where changed */
        StaticLoader.setSavedGamesChanged(true);
        /** disable him again */
        if (player)
        {
            player.GetComponent<CtrKoblizek>().setPlayable(false);
        }
    }

    /**
     * Removes saved game from LevelSerializer and FilePrefs
     **/
    public void removeGame()
    {
        LevelSerializer.SavedGames[LevelSerializer.PlayerName].RemoveAt(this.gameId);
        LevelSerializer.SaveDataToFilePrefs();
        StaticLoader.setSavedGamesChanged(true);
    }

    public void setLevel(string level)
    {
        this.level = level;
    }

    public void setData(string data)
    {
        this.data = data;
    }

    public void setGameId(int id)
    {
        this.gameId = id;
    }
}
