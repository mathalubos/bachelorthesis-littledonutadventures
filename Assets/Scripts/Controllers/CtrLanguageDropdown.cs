﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/**
 * @author Luboš Maťha
 */
public class CtrLanguageDropdown : MonoBehaviour, IPointerUpHandler, IPointerExitHandler {
    public RectTransform container;
    public Text selectedLanguage;
    public LocalizationComponent[] localizationComponents;
    private bool isOpen;

    /**
     * {@inheritdoc}
     */
    public void OnPointerUp(PointerEventData eventData)
    {
        this.isOpen = true;
    }

    /**
     * {@inheritdoc}
     */
    public void OnPointerExit(PointerEventData eventData)
    {
        this.isOpen = false;
    }

    public void OnClick(int langId)
    {
        this.isOpen = false;
        this.setMenuTitle(langId);
        StaticLoader.setLanguage(langId);
        foreach (LocalizationComponent component in this.localizationComponents)
        {
            component.langId = langId;
            component.setMenuTitle(DifficultyService.getDifficulty());
        }
    }

    private void setMenuTitle(int langId)
    {
        switch (langId)
        {
            case 1:
                this.selectedLanguage.text = "Čeština";
                break;
            case 2:
                this.selectedLanguage.text = "English";
                break;
            default:
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    private void Start()
    {
        this.isOpen = false;
        DataTable table = StaticLoader.db.ExecuteQuery("SELECT lang_id FROM Player");
        foreach (DataRow row in table.Rows)
        {
            int langId;
            if (int.TryParse(row["lang_id"].ToString(), out langId))
            {
                this.setMenuTitle(langId);
            }

            break;
        }
    }

    /**
     * {@inheritdoc}
     */
    private void Update()
    {
        Vector3 scale = this.container.localScale;
        scale.y = Mathf.Lerp(scale.y, this.isOpen ? 1 : 0, Time.deltaTime * 12);
        this.container.localScale = scale;
    }
}
