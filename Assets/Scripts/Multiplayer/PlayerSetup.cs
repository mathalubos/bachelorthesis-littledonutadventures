﻿using UnityEngine;
using UnityEngine.Networking;

/**
 * Setup script created by tutorial from Brackey to disable
 * player controll components
 * 
 * @author Luboš Maťha
 */
public class PlayerSetup : NetworkBehaviour {
    //list of components to disable
    public Behaviour[] componentsToDisable;
	// Use this for initialization
	void Start () 
    {
	    if (!this.isLocalPlayer)
        {
            foreach (Behaviour component in this.componentsToDisable)
            {
                component.enabled = false;                
            }
        }
	}
}
