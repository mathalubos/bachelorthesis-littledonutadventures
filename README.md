# Little Donut Adventures
* bachelor thesis
* platform game used to demonstrate the functionalities of Unity 5 engine

## How to install
### requirements
* Unity 5 Personal Edition

### Installation
* clone the repository
* open in Unity 5 Personal Edition

## Usage
* Run the game in game editor or build the game in build settings menu and run in your system
* Best Practice is storing large assets outside the project folder because of git commit history bloatage

## Configuration
* library for sqlite is currently located in Plugins folder of the Project folder assets, but only for x64 windows system
* for other builds, add required library from http://www.sqlite.org/download.html

## Project documentation
* Technical documentation is part of code and can be found in docblocks of relevant classes
* coding standards can be found in docs/coding-standards.txt